# Import budget data

import os
import csv

# Path to budget data

budget_data = os.path.join('usr/bin/python_challenge/PyBank/Resources/budget_data.csv')

# Initialize variables
(total-months) = 0
(total-revenue) = 0
changes = []
date-count = []
greatest-inc = 0
greatest-inc_month = 0
greatest-dec = 0
greatest-dec-month = 0

# Open budget data
with open(csvpath, newline = '') as csvfile:
    csvreader = csv.reader(csvfile, delimiter = ',')
    next(csvreader, None)
    row = next(csvreader)

# Calculate the total number of months and the total revenue
    previous-profit = (int(row[1])
    total-months = (months + 1)
    total-revenue = total-revenue + int(row[1])
    greatest-inc = int(row[1])
    greatest-inc-month = row[0]

    for row in csvreader:

        months = months + 1
        revenue = revenue + int(row[1])

        # Calculate change from current month to previous months
        change = int(row[1]) - previous-profit
        changes.append(change)
        previous-profit = int(row[1])
        date-count.append(row[0])

        # Calculate greatest increase
        if int(row[1]) > greatest-inc:
            greatest-inc = int(row[1])
            greatest-inc-month = row[0]
        
        # Calculate greatest decrease
        if int(row[1]) < greatest-dec:
            greatest-dec = int(row[1])
            greatest-dec-month = row[0]
        
        # Calculate the average of the changes and the date
        average-change = sum(changes)/len(changes)

        high = max(changes)
        low = min(changes)

        # Print the values
        print("Financial Analysis")
        print("Total Months:" + str(months))
        print("Total Amount:" + str(revenue))
        print(average-change)
        print(greatest-inc-month, max(changes))
        print(greatest-dec-month, min(changes))

    de204e9898626d9e3a7f41e0238b32c04ea7mar
        PyBank = open("output.txt","w+")
        PyBank.write("Financial Analysis")
        PyBank.write('\n' + "Total Months" + str(months))
        PyBank.write('\n' + "Total Amount" + str(revenue))
        PyBank.write('\n' + "Average" + str(average-change))
        PyBank.write('\n' + greatest-inc-month)
        PyBank.write('\n' + str(high))
        PyBank.write('\n' + greatest-dec-month)
        PyBank.write('\n' + str(low))