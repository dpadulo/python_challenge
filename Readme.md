Are you having trouble analyzing all that cumbersome information at work? Do you fear that you’re wearing out your vision or driving yourself cross-eyed manually working through sheets and sheets of data?

Consider your eyes rested with the brand-new Py Me Up office productivity package!

The PyBank code is written to sort through your financial data and the PyPoll can help analyze the the results of opinion polls or anything else you can take a vote on!

To generate the results, simply run the code from its respective folder. Results will be output into the “Analysis” folder.
