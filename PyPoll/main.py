import os
import csv

csvpath = os.path.join('usr/bin/python_challenge/PyPoll/Resources/election_data.csv')

# Initialize Variables
poll_data = {}
total_votes = 0
with open(csvpath, 'r') as csvfile:
    csvread = csv.reader(csvfile)
    next(csvread, None)

    for row in csvread:
            total_votes +=1
            if row[2] in poll_data.keys():
                poll_data[row[2]] = poll_data[row[2]] + 1
            else:
                poll_data[row[2]] = 1


candidates = []
tot_num_votes = []

# Get the Number of votes
for key, value in poll_data.items():
    candidates.append(key) 
    tot_num_votes.append(value)

# Calculate the vote percentage
percentage_votes =[]
for n in tot_num_votes:
        percentage_votes.append(round(n/total_votes * 100, 1))

# Determining the winner
clean_data =  (list(zip(candidates, tot_num_votes, percentage_votes)))

winner_list = []
for name in clean_data:
    if max(tot_num_votes) == name [1]:
        winner_list.append(name[0])
president = winner_list

# Print results
print("Election Results :")
print(total_votes)
print(candidates)
print(percentage_votes)
print(tot_num_votes)
print(president)

# Writing output files
PyPoll = open("output.txt","w+")
PyPoll.write("Election Results")
PyPoll.write('\n' + "Total_Votes" + str(total_votes))
PyPoll.write('\n' + str(candidates))
PyPoll.write('\n' + str(percentage_votes))
PyPoll.write('\n' + str(tot_num_votes))
PyPoll.write('\n' + "Winner:" + winner)
